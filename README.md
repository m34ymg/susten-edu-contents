# Public repository for susten e-learning contents

Commit your articles under the following folder structure

<pre>
articles/
  -- 1-1/
    -- main.md
    -- my_cool_chart.png  
  -- 1-2/
    -- main.md 
    -- another_cool_photo.png 
  -- 1-3/
    -- main.md  
</pre>


Pushed contents should *immediately* flow through to the [Dev Site](https://master.d2r1xqqb24pywc.amplifyapp.com). 